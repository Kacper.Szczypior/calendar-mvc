﻿using CalendarNoLogin;
using CalendarNoLogin.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;

namespace CalendarTests
{
    [TestClass]
    public class ControllerTests
    {
        public class InitedController
        {
            public HomeController HomeController { get; set; }
            public Mock<ICalendarDBData> calendarDBData { get; set; }
            public Mock<IUserDBData> userDBData { get; set; }
            public Mock<IAuthenticationManager> authenticationManager { get; set; }
        }

        public static InitedController Init()
        {
            
            var calendarDBData = new Mock<ICalendarDBData>();
            var userDBData = new Mock<IUserDBData>();
            var authentication = new Mock<IAuthenticationManager>();
            var strct = new InitedController
            {
                authenticationManager = authentication,
                calendarDBData = calendarDBData,
                userDBData = userDBData,
                HomeController = new HomeController(calendarDBData.Object, userDBData.Object, authentication.Object)
            };
            return strct;
        }


        [TestClass]
        public class Authenticate
        {
            [TestMethod]
            public void TestMethod1()
            {
                var calendarDBData = new Mock<ICalendarDBData>();
                var userDBData = new Mock<IUserDBData>();
                var authentication = new Mock<IAuthenticationManager>();
                var g = Guid.Empty;

                userDBData.Setup(x => x.GetUser("a", "b")).Returns(new Person
                {
                    PersonID = g
                });

                authentication.Setup(x => x.Authenticate(It.IsAny<HttpSessionStateBase>(), g));

                var controller = new HomeController(calendarDBData.Object, userDBData.Object, authentication.Object);

                var result = controller.Authenticate("a", "b");
                Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            }

            [TestMethod]
            public void TestFailLogin()
            {
                var calendarDBData = new Mock<ICalendarDBData>();
                var userDBData = new Mock<IUserDBData>();
                var authentication = new Mock<IAuthenticationManager>();
                var g = Guid.Empty;

                userDBData.Setup(x => x.GetUser("a", "b"));

                var controller = new HomeController(calendarDBData.Object, userDBData.Object, authentication.Object);

                var result = controller.Authenticate("a", "b");

                Assert.IsInstanceOfType(result, typeof(ViewResult));
            }
        }

        [TestClass]
        public class RemoveEvent
        {
            [TestMethod]
            public void RemoveEventCorrectTest()
            {
                var strct = Init();

                var guid = Guid.Empty;
                var timeStamp = new byte[8];

                strct.calendarDBData.Setup(x => x.RemoveById(guid, timeStamp));

                var result = strct.HomeController.RemoveEvent(guid.ToString(), TimeStampEncoder.EncodeTimeStamp(timeStamp));

                strct.calendarDBData.Verify(x => x.RemoveById(guid, timeStamp), Times.Once);
                Assert.IsInstanceOfType(result, typeof(JsonResult));

                Assert.IsInstanceOfType(result, typeof(JsonResult));
                var res = result as JsonResult;
                var s = res.Data as JQueryResult;

                Assert.AreEqual(s.success, true);
            }

            [TestMethod]
            public void RemoveEventFailTest()
            {
                var strct = Init();

                var guid = Guid.Empty;
                byte[] timeStamp = new byte[8];

                strct.calendarDBData.Setup(x => x.RemoveById(guid, timeStamp)).Throws(new Exception());

                var result = strct.HomeController.RemoveEvent(guid.ToString(), TimeStampEncoder.EncodeTimeStamp(timeStamp));

                strct.calendarDBData.Verify(x => x.RemoveById(guid, timeStamp), Times.Once);
                Assert.IsInstanceOfType(result, typeof(JsonResult));
                var res = result as JsonResult;
                var s = res.Data as JQueryResult;

                Assert.AreEqual(s.success, false);
            }
        }

        [TestClass]
        public class TestAccept
        {
            [TestMethod]
            public void TestUnlogged()
            {
                var init = Init();
                init.authenticationManager.Setup(x => x.Authenticated(It.IsAny<HttpSessionStateBase>()))
                    .Returns(false);

                init.HomeController.AcceptInvitation(Guid.Empty.ToString(), "");
            }

            [TestMethod]
            public void TestCorrectAccept()
            {
                var init = Init();
                var guid = Guid.Empty;
                var timeStamp = new byte[1];





                init.HomeController.AcceptInvitation(guid.ToString(), Convert.ToBase64String(timeStamp));
            }
        }
    }
}