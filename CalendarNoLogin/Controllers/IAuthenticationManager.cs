﻿using System;
using System.Web;

namespace CalendarNoLogin.Controllers
{
    public interface IAuthenticationManager
    {
        void Authenticate(HttpSessionStateBase session, Guid userGuid);
        bool Authenticated(HttpSessionStateBase session);
        DateTime? GetCalendarDate(HttpSessionStateBase session);
        Guid GetGuid(HttpSessionStateBase session);
        void SetCalendarDate(HttpSessionStateBase session, DateTime date);
    }
}