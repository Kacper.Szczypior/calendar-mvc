﻿using System;
using System.Web;

namespace CalendarNoLogin.Controllers
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private static readonly string authenticated = "verified";
        private static readonly string datestring = "date";
        private static readonly string guid = "userGuid";

        public void Authenticate(HttpSessionStateBase session, Guid userGuid)
        {
            session[authenticated] = true.ToString();
            session[guid] = userGuid.ToString();
        }

        public bool Authenticated(HttpSessionStateBase session)
        {
            var auth = false;
            var s = session[authenticated];
            if (s == null)
                return false;
            bool.TryParse(session[authenticated].ToString(), out auth);
            return auth;
        }

        public DateTime? GetCalendarDate(HttpSessionStateBase session)
        {
            var s = session[datestring];
            if (s == null)
                return null;
            var success = DateTime.TryParse(session[datestring].ToString(), out DateTime date);
            return success ? date : (DateTime?)null;
        }

        public Guid GetGuid(HttpSessionStateBase session)
        {
            return Guid.Parse(session[guid].ToString());
        }

        public void SetCalendarDate(HttpSessionStateBase session, DateTime date)
        {
            session[datestring] = date.ToString();
        }
    }
}