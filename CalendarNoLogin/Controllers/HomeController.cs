﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CalendarNoLogin.Controllers
{
    public class EventRequest
    {
        public string ID { get; set; }
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public string EventBegin { get; set; }
        public string EventEnd { get; set; }
        public string TimeStamp { get; set; }
        public List<string> Logins { get; set; }
    }

    public class JQueryResult
    {
        public bool success { get; set; }
        public string responseText { get; set; }
    }

    public class CalendarViewModelJson
    {
        public string Content { get; set; }
    }

    public class CalendarViewModel
    {
        public List<CalendarWeek> Weeks { get; set; } = new List<CalendarWeek>();
    }

    public class CalendarWeek
    {
        public string WeekNumber { get; set; }
        public List<CalendarDay> Days { get; set; } = new List<CalendarDay>();
    }

    public class CalendarDay
    {
        public DateTime Date { get; set; }
        public List<CalendarDayEvent> Events { get; set; } = new List<CalendarDayEvent>();
    }

    public class CalendarDayEvent
    {
        public string Begin { get; set; }
        public string End { get; set; }
        public string Guid { get; set; }
        public string Title { get; set; }
    }

    public class UserViewModel
    {
        public string login { get; set; }
        public bool selected { get; set; }
    }

    public class NewEventViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Begin { get; set; }
        public string BeginTime { get; set; }
        public string End { get; set; }
        public List<UserViewModel> AvailableUsers { get; set; }
    }

    public class EditEventViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Begin { get; set; }
        public string BeginTime { get; set; }
        public string End { get; set; }
        public List<UserViewModel> AvailableUsers { get; set; }
        public bool AcceptDecision { get; set; }
        public string EventTimeStamp { get; set; }
    }

    public static class TimeStampEncoder
    {
        public static string EncodeTimeStamp(byte[] timestamp)
        {
            return Convert.ToBase64String(timestamp);
        }

        public static byte[] DecodeTimeStamp(string timestamp)
        {
            return Convert.FromBase64String(timestamp);
        }
    }

    public class HomeController : Controller
    {

        private readonly ICalendarDBData _calendarDBData;
        private readonly IUserDBData _userDBData;
        private readonly IAuthenticationManager _authenticationManager;

        public HomeController(ICalendarDBData calendarDBData, IUserDBData userDBData, IAuthenticationManager authenticationManager)
        {
            _calendarDBData = calendarDBData;
            _userDBData = userDBData;
            _authenticationManager = authenticationManager;
        }

        public ActionResult Authenticate(string firstName, string secondName)
        {
            var person = _userDBData.GetUser(firstName, secondName);
            if (person != null)
            {
                _authenticationManager.Authenticate(Session, person.PersonID);
                return RedirectToAction(nameof(GetCalendar));
            }
            else return Index();
        }

        public ActionResult CreateNewEvent(EventRequest request)
        {
            if (!_authenticationManager.Authenticated(Session))
            {
                return Index();
            }
            var date = DateTime.ParseExact(request.EventDate, "MM/dd/yyyy", new CultureInfo("en-US"), DateTimeStyles.None);
            var beg = ParseTimeSpan(request.EventBegin);
            var end = ParseTimeSpan(request.EventEnd);
            var edit = false;
            var eventGuid = Guid.Empty;
            if (!string.IsNullOrEmpty(request.ID))
            {
                edit = Guid.TryParse(request.ID, out eventGuid);
            }
            
            var users = _userDBData.GetUsers();
            var p = new List<Person>();
            if (!edit)
            {
                foreach (var user in request.Logins)
                {
                    try
                    {
                        p.Add(users.First(x => x.UserID == user));
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }

            

            var e = new Appointment
            {
                AppointmentDate = date,
                Description = request.EventName,
                Title = request.EventName,
                StartTime = beg,
                EndTime = end,
                AppointmentID = eventGuid,
            };
            if (edit)
            {
                try
                {
                    byte[] timeStamp = null;
                    try
                    {
                        timeStamp = TimeStampEncoder.DecodeTimeStamp(request.TimeStamp);
                    }
                    catch (Exception ex)
                    {
                        return CreateJqueryResponse(false, "no timestamp sent");
                    }

                    e.timestamp = timeStamp;

                    _calendarDBData.EditEventNoAttendees(e);
                }
                catch(Exception ex)
                {
                    return CreateJqueryResponse(false, "try reloading event");
                }
            }
            else
            {
                _calendarDBData.AddEvent(e, p, _authenticationManager.GetGuid(Session));
            }
            
            return CreateJqueryResponse(true, "");
        }

        public ActionResult RemoveEvent(string guid, string timeStamp)
        {
            
            try
            {
                var g = Guid.Parse(guid);
                var t = TimeStampEncoder.DecodeTimeStamp(timeStamp);
                _calendarDBData.RemoveById(g, t);
                return CreateJqueryResponse(true, "delete successful");
            }
            catch(Exception ex)
            {
                return CreateJqueryResponse(false, "try reloading event");
            }
        }

        private JsonResult CreateJqueryResponse(bool succes, string message)
        {
            return Json(new JQueryResult
            {
                success = succes,
                responseText = message
            });
            //return Json(new { success = succes, responseText = "message" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCalendar(string start)
        {
            if (!_authenticationManager.Authenticated(Session))
                return Index();
            var userGuid = _authenticationManager.GetGuid(Session);
            var date = DateTime.Now;
            var inSessionDate = _authenticationManager.GetCalendarDate(Session);
            if (inSessionDate != null)
            {
                date = inSessionDate.Value;
            }
            var parsed = int.TryParse(start, out int offset);
            if (parsed)
            {
                date = date.AddDays(7 * offset);
            }
            _authenticationManager.SetCalendarDate(Session, date);
            var eventsFromDB = _calendarDBData.GetBetweenDatesIncluding(date, date.AddDays(28), userGuid);
            var model = new CalendarViewModel();
            for (int week = 1; week < 5; week++)
            {
                var weekmodel = new CalendarWeek
                {
                    WeekNumber = $"W{GetWeekOfYear(date)}"
                };
                for (int day = 0; day < 7; day++)
                {
                    var daymodel = new CalendarDay
                    {
                        Date = date,
                    };

                    var forDay = eventsFromDB.Where(x => x.AppointmentDate.Date == date.Date);

                    foreach (var sth in forDay)
                    {
                        daymodel.Events.Add(new CalendarDayEvent
                        {
                            Title = sth.Description,
                            End = GetTimeString(sth.EndTime),
                            Begin = GetTimeString(sth.StartTime),
                            Guid = sth.AppointmentID.ToString()
                        });
                    }

                    weekmodel.Days.Add(daymodel);
                    date = date.AddDays(1);
                }
                model.Weeks.Add(weekmodel);
            }

            var jsonModel = new CalendarViewModelJson
            {
                Content = JsonConvert.SerializeObject(model)
            };
            return View(nameof(Index), jsonModel);
        }

        public ActionResult Index()
        {
            return View("Login");
        }



        public ActionResult AcceptInvitation(string guid, string timeStamp)
        {
            if (!_authenticationManager.Authenticated(Session))
            {
                return Index();
            }
            try
            {
                var e = Guid.Parse(guid);
                var t = TimeStampEncoder.DecodeTimeStamp(timeStamp);
                _calendarDBData.AcceptInvitation(e, _authenticationManager.GetGuid(Session), t);
                return CreateJqueryResponse(true, "accepted invitation");
            }
            catch(Exception ex)
            {

            }

            return CreateJqueryResponse(false, "something went wrong");
        }


        public ActionResult DenyInvitation(string guid, string timeStamp)
        {
            if (!_authenticationManager.Authenticated(Session))
            {
                return Index();
            }
            try
            {
                var e = Guid.Parse(guid);
                var t = TimeStampEncoder.DecodeTimeStamp(timeStamp);
                _calendarDBData.DenyInvitation(e, _authenticationManager.GetGuid(Session), t);
                return CreateJqueryResponse(true, "accepted invitation");
            }
            catch (Exception ex)
            {

            }

            return CreateJqueryResponse(false, "something went wrong");
        }

        public ActionResult EditEvent(string guid)
        {
            if (!_authenticationManager.Authenticated(Session))
            {
                return Index();
            }

            var realGuid = Guid.Parse(guid);

            var editedEvent = _calendarDBData.GetById(realGuid);
            var userGuid = _authenticationManager.GetGuid(Session);
            var model = new EditEventViewModel
            {
                Begin = editedEvent.AppointmentDate.ToString(@"MM\/dd\/yyyy"),
                BeginTime = GetTimeString(editedEvent.StartTime),
                End = GetTimeString(editedEvent.EndTime),
                AvailableUsers = editedEvent.Attendances.Select(x => new UserViewModel
                {
                    login = x.Person.UserID,
                    selected = x.Accepted
                }).ToList(),
                ID = editedEvent.AppointmentID.ToString(),
                Title = editedEvent.Title,
                AcceptDecision = editedEvent.Attendances.Any( x=> x.PersonID == userGuid && x.Accepted == false),
                EventTimeStamp = TimeStampEncoder.EncodeTimeStamp(editedEvent.timestamp)
            };

            return PartialView(model);
        }

        public ActionResult NewEvent(string Date)
        {
            if (!_authenticationManager.Authenticated(Session))
            {
                return Index();
            }
            var parsed = long.TryParse(Date, out long ms);
            var date = DateTime.Now;
            if (parsed)
            {
                date = DateTimeOffset.FromUnixTimeMilliseconds(ms).DateTime;
            }


            var guid = _authenticationManager.GetGuid(Session);
            var usrs = _userDBData.GetUsers().Where(p => p.PersonID != guid);

            var model = new NewEventViewModel
            {
                AvailableUsers = usrs.Select(x => new UserViewModel
                {
                    login = x.UserID,
                    selected = false
                }).ToList(),
                Begin = date.ToString(@"MM\/dd\/yyyy"),
                BeginTime = GetTimeString(DateTime.Now.TimeOfDay),
                End = GetTimeString(DateTime.Now.TimeOfDay)
            };

            return PartialView(model);
        }

        private static string GetTimeString(TimeSpan span)
        {
            var time = "";
            var secondhalf = false;
            if (span.Hours > 12)
            {
                secondhalf = true;
                var hour = span.Hours - 12;
                time += hour;
            }
            else
            {
                time += span.Hours;
            }
            time += " ";
            time += span.Minutes;
            time += " ";
            time += secondhalf ? "PM" : "AM";
            return time;
        }

        private static int GetWeekOfYear(DateTime date)
        {
            var dfi = DateTimeFormatInfo.CurrentInfo;
            var cal = dfi.Calendar;
            return cal.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        private static TimeSpan ParseTimeSpan(string s)
        {
            return DateTime.ParseExact(s, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
        }
    }
}