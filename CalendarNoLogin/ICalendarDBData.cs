﻿using System;
using System.Collections.Generic;

namespace CalendarNoLogin
{
    public interface ICalendarDBData
    {
        void AcceptInvitation(Guid AppointmentId, Guid UserId, byte[] token);
        void AddEvent(Appointment e, List<Person> persons, Guid creator);
        void AddInvitee(Attendance att, Guid EventId);
        void DenyInvitation(Guid AppointmentId, Guid UserId, byte[] token);
        void EditEvent(Guid id, Appointment e);
        void EditEventNoAttendees(Appointment e);
        IEnumerable<Appointment> GetBetweenDatesIncluding(DateTime begin, DateTime end, Guid userid);
        Appointment GetById(Guid id);
        void RemoveById(Guid id, byte[] token);
    }
}