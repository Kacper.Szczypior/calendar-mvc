﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalendarNoLogin
{
    public class UserDBData : IUserDBData
    {
        public Person GetUser(string firstName, string lastName)
        {
            //Logger.log.Debug("validating user");
            using (var db = new entitydatamoedl())
            {
                var count = db.People.Count();

                if (count < 5)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        db.People.Add(new Person
                        {
                            FirstName = $"imie{i}",
                            LastName = "nazwisko",
                            UserID = $"imie{i}",
                            PersonID = Guid.NewGuid(),
                        });
                    }
                }
                db.SaveChanges();
                return db.People.First(p => p.FirstName == firstName && p.LastName == lastName);
            }
        }

        public IEnumerable<Person> GetUsers()
        {
            using (var db = new entitydatamoedl())
            {
                return db.People.ToList();
            }
        }
    }
}