﻿using System.Collections.Generic;

namespace CalendarNoLogin
{
    public interface IUserDBData
    {
        Person GetUser(string firstName, string lastName);
        IEnumerable<Person> GetUsers();
    }
}