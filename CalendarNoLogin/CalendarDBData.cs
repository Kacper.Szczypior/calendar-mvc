﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CalendarNoLogin
{

    public class CalendarDBData : ICalendarDBData
    {
        public void AcceptInvitation(Guid AppointmentId, Guid UserId, byte[] token)
        {
            using (var ctxt = new entitydatamoedl())
            {
                var origin = ctxt.Appointments.First(s => s.Attendances.Any(l => l.AppointmentID == AppointmentId));
                if (!CompareByteArrays(origin.timestamp, token))
                {
                    throw new EventConcurrencyException();
                }

                var toAccept = ctxt.Attendances.First(x => x.AppointmentID == AppointmentId && x.PersonID == UserId);
                toAccept.Accepted = true;
                ctxt.SaveChanges();
            }
        }

        public void AddEvent(Appointment e, List<Person> persons, Guid creator)
        {
            //Logger.log.Debug("loading model");
            using (var db = new entitydatamoedl())
            {
                foreach (var p in persons)
                {
                    var k = db.People.First(x => x.UserID == p.UserID);

                    e.Attendances.Add(new Attendance
                    {
                        AttendanceID = Guid.NewGuid(),
                        Person = k,
                        Accepted = false
                    });
                }

                e.Attendances.Add(new Attendance
                {
                    AttendanceID = Guid.NewGuid(),
                    Person = db.People.First(x => x.PersonID == creator),
                    Accepted = true
                });
                e.AppointmentID = Guid.NewGuid();
                db.Appointments.Add(e);
                db.SaveChanges();
            }
        }

        public void AddInvitee(Attendance att, Guid EventId)
        {
            using (var ctxt = new entitydatamoedl())
            {
                var eve = ctxt.Appointments.Where(e => e.AppointmentID == EventId).First();

                eve.Attendances.Add(att);

                ctxt.SaveChanges();
            }
        }

        public void DenyInvitation(Guid AppointmentId, Guid UserId, byte[] token)
        {
            using (var ctxt = new entitydatamoedl())
            {
                try
                {
                    var origin = ctxt.Appointments.First(s => s.Attendances.Any(l => l.AppointmentID == AppointmentId));
                    if (!CompareByteArrays(origin.timestamp, token))
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new EventConcurrencyException();
                }

                var toAccept = ctxt.Attendances.First(x => x.AppointmentID == AppointmentId && x.PersonID == UserId);
                ctxt.Attendances.Remove(toAccept);
                ctxt.SaveChanges();
            }
        }

        public void EditEvent(Guid id, Appointment e)
        {
            //Logger.log.Debug("loading model");
            using (var db = new entitydatamoedl())
            {
                var original = db.Appointments.First(i => i.AppointmentID == id);
                if (original == null)
                {
                    throw new EventConcurrencyException();
                }
                db.Entry(original).Collection(ori => ori.Attendances).Load();

                if (original.timestamp != e.timestamp)
                    throw new EventConcurrencyException();

                original.AppointmentDate = e.AppointmentDate;
                original.StartTime = e.StartTime;
                original.EndTime = e.EndTime;

                var itemsToDelete = original.Attendances.ToList();
                db.Attendances.RemoveRange(itemsToDelete);

                db.SaveChanges();
            }

            using (var db = new entitydatamoedl())
            {
                var original = db.Appointments.First(i => i.AppointmentID == id);
                foreach (var p in e.Attendances)
                {
                    var l = new Attendance
                    {
                        Accepted = p.Accepted,
                        Person = p.Person
                    };

                    original.Attendances.Add(l);
                }

                db.SaveChanges();
            }
        }

        public void EditEventNoAttendees(Appointment e)
        {
            using (var db = new entitydatamoedl())
            {
                var original = db.Appointments.First(i => i.AppointmentID == e.AppointmentID);
                if (original == null)
                {
                    throw new EventConcurrencyException();
                }
                //db.Entry(original).Collection(ori => ori.Attendances).Load();
                if (!CompareByteArrays(original.timestamp, e.timestamp))
                    throw new EventConcurrencyException();

                original.AppointmentDate = e.AppointmentDate;
                original.StartTime = e.StartTime;
                original.EndTime = e.EndTime;

                //var itemsToDelete = original.Attendances.ToList();
                //db.Attendances.RemoveRange(itemsToDelete);

                db.SaveChanges();
            }
        }

        public IEnumerable<Appointment> GetBetweenDatesIncluding(DateTime begin, DateTime end, Guid userid)
        {
            //Logger.log.Debug("loading model");
            using (var db = new entitydatamoedl())
            {
                var rule = Convert.ToDateTime(begin.Date);
                var rule2 = Convert.ToDateTime(end.Date);
                return db.Appointments
                    .Where(u => DbFunctions.TruncateTime(u.AppointmentDate) >= rule && DbFunctions.TruncateTime(u.AppointmentDate) <= end.Date &&
                    (u.Attendances.Any(p => p.Person.PersonID == userid)))
                    .OrderBy(e => e.AppointmentDate)
                    .ToList();
            }
        }

        public Appointment GetById(Guid id)
        {
            //Logger.log.Debug("loading model");
            using (var db = new entitydatamoedl())
            {
                var toret = db.Appointments.First(i => i.AppointmentID == id);
                db.Entry(toret).Collection(p => p.Attendances).Load();
                foreach (var e in toret.Attendances)
                {
                    db.Entry(e).Reference(k => k.Person).Load();
                }

                return toret;
            }
        }

        public void RemoveById(Guid id, byte[] token)
        {
            //Logger.log.Debug("loading model");
            using (var db = new entitydatamoedl())
            {
                Appointment eve;
                try
                {
                    eve = db.Appointments.First(s => s.AppointmentID == id);
                    bool varlid = CompareByteArrays(token, eve.timestamp);

                    if (!varlid)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new EventConcurrencyException(ex);
                }

                db.Entry(eve).Collection(k => k.Attendances).Load();

                db.Attendances.RemoveRange(eve.Attendances);

                db.Appointments.Remove(eve);

                db.SaveChanges();
            }
        }
        public void RemoveInvitee(Guid id, int token)
        {
            using (var db = new entitydatamoedl())
            {
                var att = new Attendance
                {
                    AttendanceID = id
                };
                db.Attendances.Attach(att);
                db.Attendances.Remove(att);

                db.SaveChanges();
            }
        }

        private static bool CompareByteArrays(byte[] token, byte[] eve)
        {
            var varlid = true;
            for (int i = 0; i < token.Length; i++)
            {
                if (token[i] != eve[i])
                    varlid = false;
            }

            return varlid;
        }
    }
}