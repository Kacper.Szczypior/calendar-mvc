﻿using System;

namespace CalendarNoLogin
{
    public class EventConcurrencyException : Exception
    {
        public EventConcurrencyException(Exception inner) : base("", inner) { }
        public EventConcurrencyException() : base() { }
    }

}